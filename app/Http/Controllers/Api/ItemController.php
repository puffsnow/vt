<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

use App\User;
use App\Item;


class ItemController extends Controller
{

    public function index(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $items = Item::where([
          ['user_id', $user->id],
          ['deleted_at', '=', null],
        ])->get();

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "OK",
            'data' => [
               'count' => count($items),
               'items' => $items,
            ],
        ], 200);
    }


    public function show(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $item = Item::where('id', $request->id)->first();

        if ($item == null || $item->deleted_at != null || $item->user_id != $user->id) {
          return response()->json([
              'code' => 400,
              'status' => false,
              'message' => "Item not exist."
          ], 400);
        }

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "OK",
            'data' => [
               'item' => $item,
            ],
        ], 200);
    }

    public function store(Request $request) {
        $user = JWTAuth::toUser($request->token);

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'status' => false,
                'message' => "Item create failed"
            ], 400);
        }

        $input = $request->all();
        $input['user_id'] = $user->id;
        $item = Item::create($input);
        $item->save();

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Item create success",
            'data' => [
               'item' => $item,
            ],
        ], 200);
    }

    public function update(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $item = Item::where('id', $request->id)->first();

        if ($item == null || $item->deleted_at != null || $item->user_id != $user->id) {
          return response()->json([
              'code' => 400,
              'status' => false,
              'message' => "Item not exist."
          ], 400);
        }

        $validator = Validator::make($request->all(), [
            'title' => 'required|string|max:255',
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'status' => false,
                'message' => "Item update failed"
            ], 400);
        }

        $item->title = $request->title;
        $item->content = $request->content;
        $item->attachment = $request->attachment;
        $item->save();

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Item update success",
            'data' => [
               'item' => $item,
            ]
        ], 200);
    }

    public function destroy(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $item = Item::where('id', $request->id)->first();

        if ($item == null || $item->deleted_at != null || $item->user_id != $user->id) {
          return response()->json([
              'code' => 400,
              'status' => false,
              'message' => "Item not exist."
          ], 400);
        }

        $item->deleted_at = date('Y-m-d H:i:s');
        $item->save();

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Item delete success",
        ], 200);
    }

    public function done(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $item = Item::where('id', $request->id)->first();

        if ($item == null || $item->deleted_at != null || $item->user_id != $user->id) {
          return response()->json([
              'code' => 400,
              'status' => false,
              'message' => "Item not exist."
          ], 400);
        }

        $item->done_at = date('Y-m-d H:i:s');
        $item->save();

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Item done success",
        ], 200);
    }

    public function destroy_all(Request $request) {
        $user = JWTAuth::toUser($request->token);
        $items = Item::where([
            ['user_id', $user->id],
            ['deleted_at', '=', null],
        ])->get();

        foreach ($items as $item) {
            $item->deleted_at = date('Y-m-d H:i:s');
            $item->save();
        }

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Items delete success",
        ], 200);
    }
}
