<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;

class UserController extends Controller
{
    public function register(Request $request) {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
        ]);
        
        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'status' => false,
                'message' => "Register failed"
            ], 400);
        }

        $input = $request->all();
        $user = User::create($input);
        $user->password = Hash::make($input['password']);
        $user->save();
        
        $token = auth()->attempt(['email' => $input['email'], 'password' => $input['password']]);

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Register Success",
            'token' => $token
        ], 200);
    }

    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'email' => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'code' => 400,
                'status' => false,
                'message' => "Login failed. Missing email or password."
            ], 400);
        }

        $token = auth()->attempt(['email' => $request->email, 'password' => $request->password]);

        if(!($token)){
            $token = auth()->attempt(['name' => $request->email, 'password' => $request->password]);
        }

        if(!$token){
            return response()->json([
                'code' => 400,
                'status' => false,
                'message' => "Login failed. Invalid email or password",
            ], 400);
        }

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Login success",
            'token' => $token
        ], 200);

    }

    public function logout(Request $request) {
        auth()->logout();
        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Logout success",
            'data'   => []
        ], 200);
    }

    public function refresh_token(Request $request) {
        $token = JWTAuth::getToken();

        if(!$token){
            return response()->json([
                'code' => 400,
                'status' => false,
                'message' => "Refresh failed. Invalid old token",
            ], 400);
        }

        $new_token = JWTAuth::refresh($token);

        return response()->json([
            'code'   => 200,
            'status' => true,
            'message'=> "Refresh token success",
            'token' => $new_token
        ], 200);
    }
}
