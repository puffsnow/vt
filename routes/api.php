<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['namespace' => 'Api'], function() {
    Route::get('/user/register', 'UserController@register');
    Route::get('/user/login', 'UserController@login');
    Route::get('/user/logout', 'UserController@logout');
    Route::get('/user/refresh_token', 'UserController@refresh_token');
});

Route::group(['middleware' => ['jwt.auth']], function() {
    Route::group(['namespace' => 'Api'], function() {
        Route::get('/item/index', 'ItemController@index');
        Route::get('/item/{id}', 'ItemController@show');
        Route::post('/item', 'ItemController@store');
        Route::put('/item/{id}', 'ItemController@update');
        Route::delete('/item/{id}', 'ItemController@destroy');

        Route::put('/item/done/{id}', 'ItemController@done');
        Route::delete('/item/destroy_all', 'ItemController@destroy_all');
    });
});

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
